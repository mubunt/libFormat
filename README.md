 # *libFormat*, Formatting a piece of text according to French and English typographic rules.

This library provides a routine to formatting a piece of text according to French and English typographic rules.

## EXAMPLE
Refer to [**format**](https://gitlab.com/mubunt/format) utility.

## LICENSE
**libFormat** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
*Synopsys:*
```C
char *textformat( const char *text, size_t width, eformat_alignment alignment, eformat_rule rule)
```
*Description:*

This routine formats the text *text* with a width of *width* characters, aligned on *alignment* (*LEFT*, *RIGHT* or *JUSTIFY*) respecting, as much as possible, the typographic rules *rule* (*ENGLISH*, *FRENCH*).

Some pointers to french typographic rules: http://www.typoguide.ch/tmp/, http://jacques-andre.fr/faqtypo/lessons.pdf, and http://www.irem.sciences.univ-nantes.fr/telecharge/ecrit.pdf.

*Return value:*
A pointer to a memory area containing the formatted text. This buffer must be released by the user program.

*Example:*
```C
...
#include "txtFormat.h"
...
char *line = NULL;
size_t len = 0;
char *res;
while (getline(&line, &len, stdin) != -1) {
	res = textformat( line, width, alignment, rule );
	if (res != NULL) fprintf(stdout, "%s", res);
	free(res);
}
free(line);

```

## STRUCTURE OF THE APPLICATION
This section walks you through **libFormat**'s structure. Once you understand this structure, you will easily find your way around in **libFormat**'s code base.

``` bash
$ yaTree
./                  # Application level
├── src/            # Source directory
│   ├── Makefile    # Makefile
│   ├── txtFormat.c # Source file
│   └── txtFormat.h # Header file
├── COPYING.md      # GNU General Public License markdown file
├── LICENSE.md      # License markdown file
├── Makefile        # Makefile
├── README.md       # ReadMe markdown file
├── RELEASENOTES.md # Release Notes markdown file
└── VERSION         # Version identification text file

1 directories, 9 files
$ 
```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE
```Shell
$ cd libFormat
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libFormat
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage and development, nothing particular...
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***