# RELEASE NOTES: *libFormat*, Formatting a piece of text according to French and English typographic rules..

Functional limitations, if any, of this version are described in the *README.md* file.


- **Version 2.0.9**:
  - Updated build system components.

- **Version 2.0.8**:
  - Updated build system.

- **Version 2.0.7**:
  - Removed unused files.

- **Version 2.0.6**:
  - Updated build system component(s)

- **Version 2.0.5**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 2.0.4**:
  - Some minor changes in .comment file(s).

- **Version 2.0.3**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 2.0.2**:
  - Added compilation option *-DMYALLOC* to use **libFormat** as test vehicle for **libAlloc**. Modified sources accordingly. In the end, the instrumentation is no longer active.

- **Version 2.0.1**:
  - Updated makefiles and asociated README information.

- **Version 2.0.0**:
  - Redesigned library using extended characters (*wchar_t*). Version with *char* (multi-byte string) is accessible by removing "-DWCHAR" compilation option in the top-level Makefile.

- **Version 1.0.0**:
  - First version.
