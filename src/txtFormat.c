//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFormat
// Formatting a piece of text according to French and English typographic rules.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "txtFormat.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define SPACE 				' '
#define NEWLINE 			'\n'
#define	ESCAPE 				0x1b
#define UTF8_C2				0xc2
#define UTF8_C3				0xc3
#define UTF8_E2				0xe2
#define GUILLIN				0xab
#define GUILLOUT			0xbb
#define ESC_ATTRIBUTS_OFF	"\033[0m"
#define ESCAPE_ENDCHAR		'm'

#define STORE(c, t)			{ \
								if (length_line == width  && t == VISIBLE) { \
									if ((char)c == SPACE || (char)c == NEWLINE) { \
										PUTNEWLINE(); \
										index_last_space_stored = 0; \
										length_line = 0; \
									} else { \
										if (index_last_space_stored == 0) { \
											--res.index; \
											char x = res.text[res.index]; \
											PUT('-'); \
											PUTNEWLINE(); \
											PUT(x); \
											length_line = 1; \
										} else { \
											res.text[index_last_space_stored] = NEWLINE; ++number_of_lines; \
											length_line = _count_visible_chars(res.text + index_last_space_stored, res.text + res.index); \
											index_last_space_stored = 0; \
										} \
										PUT((char)c); \
										previous_char = (char)c; \
										if (t == VISIBLE) ++length_line; \
										if ((char)c == NEWLINE) length_line = 0; \
									} \
								} else { \
									if ((char)c == SPACE) index_last_space_stored = res.index; \
									PUT((char)c); \
									previous_char = (char) c; \
									if (t == VISIBLE) ++length_line; \
									if ((char)c == NEWLINE) length_line = 0; \
								} \
							}
#define PUT(c)				res.text[res.index] = c; ++res.index
#define PUTNEWLINE()		PUT(NEWLINE); ++number_of_lines
#define MOVEBACK()			{ --res.index; }	// Never use on "non visible" char....
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum  {
	E_LETTER, E_DIGIT, E_BLANK, E_DOT, E_QUOTE, E_DASH, E_NEWLINE,
	E_PUNCTUATION_1, E_PUNCTUATION_2, E_PUNCTUATION_3, E_PUNCTUATION_4,
	E_ESCAPE, E_ESCAPE_SEQUENCE,
	E_EXTENDED_1, E_EXTENSION_1_SEQUENCE,
	E_EXTENDED_2, E_EXTENSION_2_SEQUENCE,
	E_EXTENDED_3, E_EXTENSION_31_SEQUENCE, E_EXTENSION_32_SEQUENCE,
	E_OTHER
} eformat_char;
typedef enum  {
	NO_SPECIAL, ESCAPE_ONGOING, EXTENSION_1_ONGOING, EXTENSION_2_ONGOING,
	EXTENSION_31_ONGOING, EXTENSION_32_ONGOING
} ecurrent_state;
typedef enum  {
	VISIBLE, NON_VISIBLE
} etype_char;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_text {
	char *text;
	size_t index;
};
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static char _toupperAccentued( char c ) {
// à	small letter A with grave (u+00e0)		c3a0 --> À	capital letter a with grave (u+00c0)		c380
// á	small letter A with acute (u+00e1)		c3a1 --> Á	capital letter a with acute (u+00c1)		c381
// â	small letter A with circumflex (u+00e2)	c3a2 --> Â	capital letter a with circumflex (u+00c2)	c382
// è	small letter E with grave (u+00e8)		c38a --> È	capital letter e with grave (u+00c8)		c388
// é	small letter E with acute (u+00e9)		c3a9 --> É	capital letter e with acute (u+00c9)		c389
// ê	small letter E with circumflex (u+00ea)	c3aa --> Ê	capital letter e with circumflex (u+00ca)	c38a
// ë	small letter E with diaeresis (u+00eb)	c3ab --> Ë	capital letter e with diaeresis (u+00cb)	c38b
// î	small letter I with circumflex (u+00ee)	c3ae --> Î	capital letter i with circumflex (u+00ce)	c38e
// ï	small letter I with diaeresis (u+00ef)	c3af --> Ï	capital letter i with diaeresis (u+00cf)	c38f
// ô	small letter O with circumflex (u+00f4)	c3b4 --> Ô	capital letter o with circumflex (u+00d4)	c394
// ù	small letter U with grave (u+00f9)		c3b9 --> Ù	capital letter u with grave (u+00d9)		c399
	if ((unsigned char)c >= 0xa0 && (unsigned char)c <= 0xb9)
		return (char)((unsigned int)c - 0x20);
	return c;
}

static size_t _count_visible_chars( char *from, char *to ) {
	size_t n = 0;
	while (from < to) {
		if ((unsigned char)*from == UTF8_C3 || (unsigned char)*from == UTF8_C2) ;
		else if ((unsigned char)*from == UTF8_E2) ++from;
		else if (*from == ESCAPE) {
			++from;
			while (*from != ESCAPE_ENDCHAR && from < to ) ++from;
		} else ++n;
		++from;
	}
	return n - 1;
}

static eformat_char _whatIsThisChar( char c, eformat_rule rule, ecurrent_state state ) {
	if (state == ESCAPE_ONGOING) return E_ESCAPE_SEQUENCE;
	if (state == EXTENSION_1_ONGOING) return E_EXTENSION_1_SEQUENCE;
	if (state == EXTENSION_2_ONGOING) return E_EXTENSION_2_SEQUENCE;
	if (state == EXTENSION_31_ONGOING) return E_EXTENSION_31_SEQUENCE;
	if (state == EXTENSION_32_ONGOING) return E_EXTENSION_32_SEQUENCE;

	if ((unsigned char)c == UTF8_C3) return E_EXTENDED_1;
	if ((unsigned char)c == UTF8_C2) return E_EXTENDED_2;
	if ((unsigned char)c == UTF8_E2) return E_EXTENDED_3;

	if (c == ESCAPE) return E_ESCAPE;
	if (c == NEWLINE) return E_NEWLINE;
	if (c == '.') return E_DOT;
	if (c == '-') return E_DASH;
	if (c == '"' || c == '`') return E_QUOTE;
	if (c == '\'') return E_DIGIT;	// !!!!!
	//if (c == '"' || c == '\'' || c == '`') return E_QUOTE;
	if (isblank(c)) return E_BLANK;
	if (isalpha(c)) return E_LETTER;
	if (isdigit(c) || isxdigit(c)) return E_DIGIT;
	if (ispunct(c)) {
		if (c == ':' || c == '!' || c == '?' || c == '$') {
			if (rule == ENGLISH)
				// No blank before, one blank after
				return E_PUNCTUATION_3;
			else
				// one blank before and after:
				return E_PUNCTUATION_1;
		}
		// One blank before, no blank after
		if (c == '(' || c == '[' || c == '{' || c == '#' || c == '&' || c == '^' || c == '~') return E_PUNCTUATION_2;
		// No blank before, one blank after
		if (c == ')' || c == ']' || c == '}' || c == '*' || c == '%' || c == ',' || c == ';') return E_PUNCTUATION_3;
		// No blank before and after
		if (c == '/' || c == '+' || c == '=' || c == '@' || c == '\\') return E_PUNCTUATION_4;
		if (c == '_' || c == '|' || c == '*' || c == '<' || c == '>') return E_PUNCTUATION_4;
	}
	return E_OTHER;
}
//------------------------------------------------------------------------------
// LIBRARY FUNCTIONS
//------------------------------------------------------------------------------
char *textformat( const char *text,
                  size_t width,
                  eformat_alignment alignment,
                  eformat_rule rule) {
	// Phase 1: Rewrite of the text, left-aligned, respecting the implemented formatting rules.
	struct s_text res;

	size_t alloc_increment = width * 2;
	size_t alloc_len = ((strlen(text) / width) + 1) * width;

	res.text = malloc(alloc_len * sizeof(char));
	if (res.text == NULL) return NULL;
	res.index = 0;

	char *ptin = (char *) text;
	char previous_char = '\0';
	bool start_sentence = true;
	bool start_alinea = true;
	bool ignore_blank = true;
	bool quote_ongoing = false;
	bool need_a_space_after_escape = false;
	size_t length_line = 0;
	size_t index_last_space_stored = 0;
	size_t number_of_lines = 0;
	eformat_char type;
	ecurrent_state state = NO_SPECIAL;

	for (size_t i = 0; i < strlen(text); i++) {
		if (res.index >= alloc_len - width) {
			alloc_len += alloc_increment;
			res.text = realloc(res.text, alloc_len * sizeof(char));
			if (res.text == NULL) return NULL;
		}
		if (start_alinea) {
			if (rule == FRENCH) {
				STORE(SPACE, VISIBLE)
				STORE(SPACE, VISIBLE)
			}
			start_alinea = false;
			index_last_space_stored = 0;
		}
		type = _whatIsThisChar(ptin[i], rule, state);
		if (ignore_blank && type == E_BLANK)
			continue;
		ignore_blank = false;
		switch (type) {
		case E_LETTER:		// Letter
			if (start_sentence) {
				STORE((char)toupper((int)ptin[i]), VISIBLE)
				start_sentence = false;
			} else {
				STORE(ptin[i], VISIBLE)
			}
			break;
		case E_DIGIT:			// Digit or Hexa digit
			STORE(ptin[i], VISIBLE)
			break;
		case E_BLANK:			// Blank/space or tab
			if (quote_ongoing) {
				STORE(ptin[i], VISIBLE)
			} else {
				if (previous_char != SPACE) {
					STORE(SPACE, VISIBLE)
				}
			}
			break;
		case E_DOT:			// Dot
			if (previous_char == SPACE) {
				MOVEBACK()
			}
			STORE(ptin[i], VISIBLE)
			STORE(SPACE, VISIBLE)
			ignore_blank = true;
			start_sentence = true;
			break;
		case E_DASH:			// Dash
			if (start_sentence || start_alinea) {
				STORE(ptin[i], VISIBLE)
				STORE(SPACE, VISIBLE)
				ignore_blank = true;
				start_sentence = start_alinea = false;
			} else {
				if (previous_char == SPACE) {
					MOVEBACK()
				}
				STORE(ptin[i], VISIBLE)
				ignore_blank = true;
			}
			break;
		case E_QUOTE:			// Guillemets
			if (rule == FRENCH) {
				if (quote_ongoing) {
					STORE(UTF8_C2, NON_VISIBLE)
					STORE(GUILLOUT, VISIBLE)
				} else {
					if (previous_char != SPACE) {
						STORE(SPACE, VISIBLE)
					}
					STORE(UTF8_C2, NON_VISIBLE)
					STORE(GUILLIN, VISIBLE)
				}
			} else {
				STORE(ptin[i], VISIBLE)
			}
			quote_ongoing = !quote_ongoing;
			break;
		case E_NEWLINE:		// Return
			STORE(ptin[i], NON_VISIBLE)
			++number_of_lines;
			start_sentence = true;
			if ((previous_char == NEWLINE)) start_alinea = true;
			break;
		case E_PUNCTUATION_1:	// Blank before and after
			if (previous_char != SPACE) {
				STORE(SPACE, VISIBLE)
			}
			STORE(ptin[i], VISIBLE)
			STORE(SPACE, VISIBLE)
			break;
		case E_PUNCTUATION_2:	// Blank before, no after
			if (previous_char != SPACE) {
				STORE(SPACE, VISIBLE)
			}
			STORE(ptin[i], VISIBLE)
			ignore_blank = true;
			break;
		case E_PUNCTUATION_3:	// Blank after, no before
			if (previous_char == SPACE) {
				MOVEBACK()
			}
			STORE(ptin[i], VISIBLE)
			STORE(SPACE, VISIBLE)
			break;
		case E_PUNCTUATION_4:	// No blank after and before:
			if (previous_char == SPACE) {
				MOVEBACK()
			}
			STORE(ptin[i], VISIBLE)
			ignore_blank = true;
			break;
		case E_ESCAPE:		// Escape
			need_a_space_after_escape = false;
			if (strncmp(text + i, ESC_ATTRIBUTS_OFF, strlen(ESC_ATTRIBUTS_OFF)) == 0) {
				if (previous_char == SPACE) {
					MOVEBACK()
					need_a_space_after_escape = true;
				}
			}
			STORE(ptin[i], NON_VISIBLE)
			state = ESCAPE_ONGOING;
			break;
		case E_ESCAPE_SEQUENCE:
			STORE(ptin[i], NON_VISIBLE)
			if (ptin[i] == ESCAPE_ENDCHAR) {
				state = NO_SPECIAL;
				if (need_a_space_after_escape) {
					STORE(SPACE, VISIBLE)
					need_a_space_after_escape = false;
				}
			}
			break;
		case E_EXTENDED_1:	// Accentued letters 2 bytes
			STORE(ptin[i], NON_VISIBLE)
			state = EXTENSION_1_ONGOING;
			break;
		case E_EXTENSION_1_SEQUENCE:
			if (start_sentence) {
				STORE(_toupperAccentued(ptin[i]), VISIBLE)
				start_sentence = false;
			} else {
				STORE(ptin[i], VISIBLE)
			}
			state = NO_SPECIAL;
			break;
		case E_EXTENDED_2:	// Special 2 bytes
			STORE(ptin[i], NON_VISIBLE)
			state = EXTENSION_2_ONGOING;
			break;
		case E_EXTENSION_2_SEQUENCE:
			STORE(ptin[i], VISIBLE)
			state = NO_SPECIAL;
			break;
		case E_EXTENDED_3:	// Special 3 bytes
			STORE(ptin[i], NON_VISIBLE)
			state = EXTENSION_31_ONGOING;
			break;
		case E_EXTENSION_31_SEQUENCE:
			STORE(ptin[i], NON_VISIBLE)
			state = EXTENSION_32_ONGOING;
			break;
		case E_EXTENSION_32_SEQUENCE:
			STORE(ptin[i], VISIBLE)
			state = NO_SPECIAL;
			break;
		case E_OTHER:			// Other such as control.... Ignore it....
			break;
		}
	}
	PUT('\0');
	res.text = realloc(res.text, (res.index + 1) * sizeof(char));

	// Phase 2: If required alignment is left, exit. Else Text segmentation according to the desired width.
	// LEFT
	if (alignment == LEFT)
		return res.text;

	// RIGHT or JUSIFY
	struct s_text res2;
	alloc_increment = width * 2;
	alloc_len = (number_of_lines + 1) * width;
	res2.text = malloc(alloc_len * sizeof(char));
	if (res2.text == NULL) {
		free(res.text);
		return NULL;
	}
	res2.index = 0;

	char *res3 = malloc((width * 2) * sizeof(char)); // Real need: (nb_invisible_chars + width + 1)
	if (res3 == NULL) {
		free(res.text);
		free(res2.text);
		return NULL;
	}

	char *pttext = ptin = res.text;
	while (*ptin != '\0') {
		if (res2.index >= alloc_len - width) {
			alloc_len += alloc_increment;
			res2.text = realloc(res2.text, alloc_len * sizeof(char));
			if (res2.text == NULL) {
				free(res.text);
				free(res3);
				return NULL;
			}
		}
		// For each line...
		size_t nb_invisible_chars = 0;
		size_t number_of_spaces = 0;
		// 1) Count spaces, invisible chars, etc.
		while (*ptin != '\0' && *ptin != NEWLINE) {
			if ((*ptin == SPACE) && ((rule != FRENCH) || (rule == FRENCH && ptin != pttext && ptin != pttext + 1)))
				++number_of_spaces;
			else {
				if ((unsigned char)*ptin == UTF8_C3 || (unsigned char)*ptin == UTF8_C2) ++nb_invisible_chars;
				else if ((unsigned char)*ptin == UTF8_E2) nb_invisible_chars += 2;
				else if (*ptin == ESCAPE) {
					++nb_invisible_chars;
					while (*ptin != ESCAPE_ENDCHAR) {
						++nb_invisible_chars;
						++ptin;
					}
				}
			}
			++ptin;
		}
		// 2) Remove trailing spaces (after dot for example)
		char last_char = *ptin;
		char *ptin2 = ptin - 1;
		while (*ptin2 == SPACE) {
			--ptin2;
			--number_of_spaces;
		}
		*++ptin2 = '\0';
		// 3) RIGHT or JUSTIFY
		if (last_char == '\0') {
			sprintf(res2.text + res2.index, "%s", pttext);
		} else {
			// RIGHT
			if (alignment == RIGHT) {
				sprintf(res2.text + res2.index, "%*s\n", (int)(nb_invisible_chars + width), pttext);
				++ptin;
			} else {
				// JUSTIFY
				if (number_of_spaces == 0) {
					if (last_char == '\0')
						sprintf(res2.text + res2.index, "%s", pttext);
					else {
						sprintf(res2.text + res2.index, "%-*s\n", (int)(nb_invisible_chars + width), pttext);
						++ptin;
					}
				} else {
					size_t number_of_spaces_to_distribute = width - (strlen(pttext) - nb_invisible_chars);
					if (number_of_spaces_to_distribute == 0 || number_of_spaces_to_distribute > width / 3) {
						if (last_char == '\0')
							sprintf(res2.text + res2.index, "%s", pttext);
						else {
							sprintf(res2.text + res2.index, "%-*s\n", (int)(nb_invisible_chars + width), pttext);
							++ptin;
						}
					} else {
						char *ptinterm = res3;
						ptin2 = pttext;
						while (*ptin2 != '\0') {
							if (number_of_spaces != 0 && *ptin2 == SPACE &&
							        ((rule != FRENCH) || (rule == FRENCH && ptin2 != pttext && ptin2 != pttext + 1))) {
								size_t number_of_spaces_per_space = number_of_spaces_to_distribute / number_of_spaces;
								if (number_of_spaces_to_distribute % number_of_spaces != 0) ++number_of_spaces_per_space;
								while (*ptin2 == SPACE) {
									*ptinterm = *ptin2;
									++ptinterm;
									++ptin2;
								}
								if (*ptin2 != '\0') {
									for (size_t j = 0; j < number_of_spaces_per_space; j++) {
										*ptinterm = SPACE;
										++ptinterm;
										--number_of_spaces_to_distribute;
									}
									--number_of_spaces;
								}
							}
							*ptinterm = *ptin2;
							++ptinterm;
							if (*ptin2 != '\0') ++ptin2;
						}
						*ptinterm = '\0';
						if (last_char == '\0') {
							sprintf(res2.text + res2.index, "%-s", res3);
						} else {
							sprintf(res2.text + res2.index, "%-*s\n", (int)(nb_invisible_chars + width), res3);
							++ptin;
						}
					}
				}
			}
		}
		res2.index += width + nb_invisible_chars + 1;
		pttext = ptin;
	}
	res2.text = realloc(res2.text, (res2.index + 1) * sizeof(char));
	free(res.text);
	free(res3);
	return(res2.text);
}
//------------------------------------------------------------------------------
